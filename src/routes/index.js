const jobs = require("./jobs");
const candidates = require("./candidates");
const bookmarks = require("./bookmarks");
const companies = require("./companies");

module.exports = {
  jobs,
  candidates,
  bookmarks,
  companies,
};
