const { Router } = require("express");
const { companyMS } = require("../database/microservices");
const crypto = require("crypto");
const { isAuthenticated, isAuthorizated } = require("../auth/middlewares");

const router = Router();

router.get(
  "/",
  isAuthenticated,
  isAuthorizated(["admin"]),
  async (req, res) => {
    companyMS.init();
    const list = await companyMS.get();
    companyMS.end();

    res.status(200).send(list);
  }
);

router.get(
  "/:id",
  isAuthenticated,
  isAuthorizated(["admin"]),
  async (req, res) => {
    const { id } = req.params;

    companyMS.init();
    const item = await companyMS.getById(id);
    companyMS.end();

    res.status(200).send(item);
  }
);

router.post(
  "/",
  isAuthenticated,
  isAuthorizated(["admin"]),
  async (req, res) => {
    const { email, full_name, password } = req.body;

    crypto.randomBytes(16, (err, salt) => {
      const newSalt = salt.toString("base64");
      crypto.pbkdf2(password, newSalt, 100, 64, "sha1", async (err, key) => {
        const encryptedPassword = key.toString("base64");

        companyMS.init();
        const company = await companyMS.getByEmail(email);
        companyMS.end();

        if (company) {
          return res.status(502).send("Company email is already used");
        }

        companyMS.init();
        const newCompany = await companyMS.store({
          full_name,
          email,
          password: encryptedPassword,
          salt: newSalt,
        });
        companyMS.end();

        res.status(200).send(newCompany);
      });
    });
  }
);

router.put(
  "/:id",
  isAuthenticated,
  isAuthorizated(["admin"]),
  async (req, res) => {
    const { id } = req.params;

    companyMS.init();
    await companyMS.update(id, req.body);
    companyMS.end();

    res.sendStatus(204);
  }
);

router.delete(
  "/:id",
  isAuthenticated,
  isAuthorizated(["admin"]),
  async (req, res) => {
    const { id } = req.params;

    companyMS.init();
    await companyMS.remove(id);
    companyMS.end();

    res.sendStatus(204);
  }
);

module.exports = router;
