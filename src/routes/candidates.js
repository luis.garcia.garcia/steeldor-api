const { Router } = require("express");
const { candidateMS } = require("../database/microservices");
const { isAuthenticated, isAuthorizated } = require("../auth/middlewares");

const router = Router();

router.get("/", isAuthenticated, async (req, res) => {
  candidateMS.init();
  const list = await candidateMS.get(req.body);
  candidateMS.end();

  res.status(200).send(list);
});

router.get("/:id", isAuthenticated, async (req, res) => {
  const { id } = req.params;

  candidateMS.init();
  const item = await candidateMS.getById(id);
  candidateMS.end();

  res.status(200).send(item);
});

router.get("/applied/:user_id", isAuthenticated, async (req, res) => {
  const { user_id } = req.params;

  candidateMS.init();
  const list = await candidateMS.getJobsApplied(user_id);
  candidateMS.end();

  res.status(200).send(list);
});

router.post("/", isAuthenticated, async (req, res) => {
  candidateMS.init();
  const item = await candidateMS.store(req.body);
  candidateMS.end();

  res.status(200).send(item);
});

router.delete(
  "/:id",
  isAuthenticated,
  isAuthorizated(["admin", "company"]),
  async (req, res) => {
    const { id } = req.params;

    candidateMS.init();
    await candidateMS.remove(id);
    candidateMS.end();

    res.sendStatus(204);
  }
);

module.exports = router;
