const { Router } = require("express");
const { bookmarkMS } = require("../database/microservices");
const { isAuthenticated } = require("../auth/middlewares");

const router = Router();

router.get("/:user_id", isAuthenticated, async (req, res) => {
  const { user_id } = req.params;

  bookmarkMS.init();
  const list = await bookmarkMS.get(user_id);
  bookmarkMS.end();

  res.status(200).send(list);
});

router.post("/", isAuthenticated, async (req, res) => {
  bookmarkMS.init();
  const item = await bookmarkMS.store(req.body);
  bookmarkMS.end();

  res.status(200).send(item);
});

router.put("/:id", isAuthenticated, async (req, res) => {
  const { id } = req.params;

  bookmarkMS.init();
  await bookmarkMS.update(id, req.body);
  bookmarkMS.end();

  res.sendStatus(204);
});

router.delete("/:id", isAuthenticated, async (req, res) => {
  const { id } = req.params;

  bookmarkMS.init();
  await bookmarkMS.remove(id);
  bookmarkMS.end();

  res.sendStatus(204);
});

module.exports = router;
