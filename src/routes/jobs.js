const { Router } = require("express");
const { jobMS } = require("../database/microservices");
const { isAuthenticated, isAuthorizated } = require("../auth/middlewares");

const router = Router();

router.get("/", async (req, res) => {
  jobMS.init();
  const list = await jobMS.get();
  jobMS.end();

  res.status(200).send(list);
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;

  jobMS.init();
  const item = await jobMS.getById(id);
  jobMS.end();

  res.status(200).send(item);
});

router.get("/company/:company_id", isAuthenticated, async (req, res) => {
  const { company_id } = req.params;

  jobMS.init();
  const item = await jobMS.getCompanyJobs(company_id);
  jobMS.end();

  res.status(200).send(item);
});

router.post(
  "/",
  isAuthenticated,
  isAuthorizated(["admin", "company"]),
  async (req, res) => {
    jobMS.init();
    const item = await jobMS.store(req.body);
    jobMS.end();

    res.status(200).send(item);
  }
);

router.put(
  "/:id",
  isAuthenticated,
  isAuthorizated(["admin", "company"]),
  async (req, res) => {
    const { id } = req.params;

    jobMS.init();
    await jobMS.update(id, req.body);
    jobMS.end();

    res.sendStatus(204);
  }
);

router.delete(
  "/:id",
  isAuthenticated,
  isAuthorizated(["admin", "company"]),
  async (req, res) => {
    const { id } = req.params;

    jobMS.init();
    await jobMS.remove(id);
    jobMS.end();

    res.sendStatus(204);
  }
);

module.exports = router;
