const { Router } = require("express");
const { userMS } = require("../database/microservices");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const { isAuthenticated } = require("../auth/middlewares");

const router = Router();

const signToken = (id) => {
  const { PRIVATE_KEY } = process.env;

  return jwt.sign({ id }, PRIVATE_KEY, {
    expiresIn: 3600,
  });
};

router.post("/register", (req, res) => {
  const { full_name, email, password, role = "user" } = req.body;

  crypto.randomBytes(16, (err, salt) => {
    const newSalt = salt.toString("base64");
    crypto.pbkdf2(password, newSalt, 100, 64, "sha1", async (err, key) => {
      const encryptedPassword = key.toString("base64");

      userMS.init();
      const user = await userMS.getByEmail(email);
      userMS.end();

      if (user) {
        return res.status(502).send("User already exists");
      }

      userMS.init();
      const newUser = await userMS.store({
        full_name,
        email,
        password: encryptedPassword,
        salt: newSalt,
        role,
      });
      userMS.end();

      const token = signToken(newUser.id);
      res.status(200).send({ token });
    });
  });
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  userMS.init();
  const user = await userMS.getByEmail(email);
  userMS.end();

  if (!user) {
    return res.status(402).send("Email or Password incorrect");
  }

  crypto.pbkdf2(password, user.salt, 100, 64, "sha1", (err, key) => {
    const encryptedPassword = key.toString("base64");
    if (user.password === encryptedPassword) {
      const token = signToken(user.id);
      return res.status(200).send({ token });
    }
    res.status(402).send("Email or Password incorrect");
  });
});

router.get("/", isAuthenticated, (req, res) => {
  res.send(req.user);
});

router.get("/logout", isAuthenticated, (req, res) => {
  res.send({ token: "" });
});

module.exports = router;
