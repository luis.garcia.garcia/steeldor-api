const isAuthorizated = (roles) => (req, res, next) => {
  if (!!req.user && roles.indexOf(req.user.role) > -1) {
    return next();
  }
  res.sendStatus(403);
};

module.exports = isAuthorizated;
