const jwt = require("jsonwebtoken");
const { userMS } = require("../../database/microservices");

const isAuthenticated = (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) {
    return res.sendStatus(403);
  }

  const { PRIVATE_KEY } = process.env;

  jwt.verify(token, PRIVATE_KEY, async (err, decoded) => {
    if (err) return res.sendStatus(403);
    if (!decoded.id) return res.sendStatus(403);
    const { id } = decoded;

    userMS.init();
    const user = await userMS.getById(id);
    userMS.end();

    if (!user) return res.sendStatus(403);
    req.user = user;

    next();
  });
};

module.exports = isAuthenticated;
