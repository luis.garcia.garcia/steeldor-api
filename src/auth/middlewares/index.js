const isAuthenticated = require("./isAuthenticated");
const isAuthorizated = require("./isAuthorizated");

module.exports = {
  isAuthenticated,
  isAuthorizated,
};
