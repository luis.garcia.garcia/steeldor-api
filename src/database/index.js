const waitPort = require("wait-port");
const mysql = require("mysql2");

const { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;

let pool;

async function init() {
  const poolOptions = {
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
    connectionLimit: 5,
  };
  await waitPort({ host: MYSQL_HOST, port: 3306, timeout: 15000 });

  pool = mysql.createPool(poolOptions);

  // CREATES USERS TABLE
  return new Promise((res, rej) => {
    pool.query(
      `CREATE TABLE IF NOT EXISTS users (
        id int not null auto_increment,
        full_name varchar(100) not null,
        email varchar(100) not null unique,
        password varchar(255) not null,
        role enum('user','admin','company') default 'user',
        salt varchar(255) not null,
        created_at timestamp not null default current_timestamp,
        updated_at timestamp null on update current_timestamp,
        primary key (id)
        )`,
      (err) => {
        if (err) return rej(err);
      }
    );

    // CREATES JOBS TABLE
    pool.query(
      `CREATE TABLE IF NOT EXISTS jobs (
        id int not null auto_increment,
        company_name varchar(100) not null,
        location varchar(50) not null,
        description varchar(255) not null,
        salary_range varchar(50) not null,
        skills varchar(255) not null,
        company_id int not null,
        created_at timestamp not null default current_timestamp,
        updated_at timestamp null on update current_timestamp,
        primary key (id),
        foreign key (company_id) references users(id)
        )`,
      (err) => {
        if (err) return rej(err);
      }
    );

    // CREATES BOOKMARKS TABLE
    pool.query(
      `CREATE TABLE IF NOT EXISTS bookmarks (
        id int not null auto_increment,
        job_id int not null,
        user_id int not null,
        created_at timestamp not null default current_timestamp,
        updated_at timestamp null on update current_timestamp,
        primary key (id),
        foreign key (job_id) references jobs(id),
        foreign key (user_id) references users(id)
        )`,
      (err) => {
        if (err) return rej(err);
      }
    );

    // CREATES CANDIDATES TABLE
    pool.query(
      `CREATE TABLE IF NOT EXISTS candidates (
        id int not null auto_increment,
        job_id int not null,
        user_id int not null,
        resume varchar(100) not null,
        name varchar(100) not null,
        email varchar(100) not null,
        created_at timestamp not null default current_timestamp,
        updated_at timestamp null on update current_timestamp,
        primary key (id),
        foreign key (job_id) references jobs(id),
        foreign key (user_id) references users(id)
        )`,
      (err) => {
        if (err) return rej(err);
        res();
      }
    );
  });
}

async function teardown() {
  return new Promise((res, rej) => {
    pool.end((err) => {
      if (err) rej(err);
      else res();
    });
  });
}

module.exports = {
  init,
  teardown,
};
