const userMS = require("./user");
const candidateMS = require("./candidate");
const jobMS = require("./job");
const bookmarkMS = require("./bookmark");
const companyMS = require("./company");

module.exports = {
  userMS,
  candidateMS,
  jobMS,
  bookmarkMS,
  companyMS,
};
