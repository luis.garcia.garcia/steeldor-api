const mysql = require("mysql2");
let pool, promisePool;

const init = () => {
  let { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;
  const poolOptions = {
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
  };

  pool = mysql.createPool(poolOptions);
  promisePool = pool.promise();
};

const end = () => {
  pool.end();
};

const get = async () => {
  const [rows, fields] = await promisePool.query("SELECT * FROM jobs");

  return rows;
};

const getById = async (id) => {
  const [rows, fields] = await promisePool.query(
    "SELECT * FROM jobs WHERE id = ? LIMIT 1",
    [id]
  );

  return rows[0];
};

const getCompanyJobs = async (company_id) => {
  const [rows, fields] = await promisePool.query(
    "SELECT * FROM jobs WHERE company_id = ?",
    [company_id]
  );

  return rows;
};

const store = async (item) => {
  const [rows, fields] = await promisePool.query(
    "INSERT INTO jobs (company_name,location,description,salary_range,skills,company_id) values(?,?,?,?,?,?)",
    [
      item.company_name,
      item.location,
      item.description,
      item.salary_range,
      item.skills,
      item.company_id,
    ]
  );

  return { ...item, id: rows.insertId };
};

const update = async (id, item) => {
  return ([rows, fields] = await promisePool.query(
    "UPDATE jobs SET company_name=?, location=?, description=?, salary_range=?, skills=?, company_id=? WHERE id=?",
    [
      item.company_name,
      item.location,
      item.description,
      item.salary_range,
      item.skills,
      item.company_id,
      id,
    ]
  ));
};

const remove = async (id) => {
  return ([rows, fields] = await promisePool.query(
    "DELETE FROM jobs WHERE id=?",
    [id]
  ));
};

module.exports = {
  init,
  end,
  get,
  getById,
  getCompanyJobs,
  store,
  update,
  remove,
};
