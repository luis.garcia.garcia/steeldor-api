const mysql = require("mysql2");
let pool, promisePool;

const init = () => {
  let { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;
  const poolOptions = {
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
  };

  pool = mysql.createPool(poolOptions);
  promisePool = pool.promise();
};

const end = () => {
  pool.end();
};

const get = async () => {
  const [rows, fields] = await promisePool.query(
    "SELECT users.id, users.full_name, users.email FROM users WHERE role='company'"
  );

  return rows;
};

const getById = async (id) => {
  const [rows, fields] = await promisePool.query(
    "SELECT users.id, users.full_name, users.email FROM users WHERE id=? and role='company' LIMIT 1",
    [id]
  );

  return rows[0];
};

const getByEmail = async (email) => {
  const [rows, fields] = await promisePool.query(
    "SELECT users.id, users.full_name, users.email FROM users WHERE email = ? LIMIT 1",
    [email]
  );

  return rows[0];
};

const store = async (item) => {
  const [rows, fields] = await promisePool.query(
    "INSERT INTO users (full_name,email,password,role,salt) values(?,?,?,?,?)",
    [item.full_name, item.email, item.password, "company", item.salt]
  );

  return { ...item, id: rows.insertId };
};

const update = async (id, item) => {
  return ([rows, fields] = await promisePool.query(
    "UPDATE users SET full_name=? WHERE id=? and role='company'",
    [item.full_name, id]
  ));
};

const remove = async (id) => {
  return ([rows, fields] = await promisePool.query(
    "DELETE FROM users WHERE id=? and role='company'",
    [id]
  ));
};

module.exports = {
  init,
  end,
  get,
  getById,
  getByEmail,
  store,
  update,
  remove,
};
