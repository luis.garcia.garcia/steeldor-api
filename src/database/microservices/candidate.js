const mysql = require("mysql2");
let pool, promisePool;

const init = () => {
  let { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;
  const poolOptions = {
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
  };

  pool = mysql.createPool(poolOptions);
  promisePool = pool.promise();
};

const end = () => {
  pool.end();
};

const get = async (filter = null) => {
  let params = [];
  let query = "SELECT * FROM candidates";

  if (filter.job_id) {
    query += " WHERE job_id = ?";
    params = params.concat([filter.job_id]);
  }
  if (filter.user_id) {
    query += params.length > 0 ? " AND user_id = ?" : " WHERE user_id = ?";
    params = params.concat([filter.user_id]);
  }
  if (filter.name) {
    query += params.length > 0 ? " AND name = ?" : " WHERE name = ?";
    params = params.concat([filter.name]);
  }
  if (filter.email) {
    query += params.length > 0 ? " AND email = ?" : " WHERE email = ?";
    params = params.concat([filter.email]);
  }

  const [rows, fields] = await promisePool.query(query, params);

  return rows;
};

const getById = async (id) => {
  const [rows, fields] = await promisePool.query(
    "SELECT * FROM candidates WHERE id = ? LIMIT 1",
    [id]
  );

  return rows[0];
};

const getJobsApplied = async (user_id) => {
  const [rows, fields] = await promisePool.query(
    `SELECT 
      job.company_name,
      job.location,
      job.description,
      job.salary_range,
      job.skills,
      job.company_id,
      job.id as job_id, 
      candidate.id 
      FROM candidates candidate RIGHT JOIN jobs job ON job.id = candidate.job_id 
      WHERE candidate.user_id = ?`,
    [user_id]
  );

  return rows;
};

const store = async (item) => {
  const [rows, fields] = await promisePool.query(
    "INSERT INTO candidates (job_id,user_id,resume,name,email) values(?,?,?,?,?)",
    [item.job_id, item.user_id, item.resume, item.name, item.email]
  );

  return { ...item, id: rows.insertId };
};

const remove = async (id) => {
  return ([rows, fields] = await promisePool.query(
    "DELETE FROM candidates WHERE id=?",
    [id]
  ));
};

module.exports = {
  init,
  end,
  get,
  getById,
  getJobsApplied,
  store,
  remove,
};
