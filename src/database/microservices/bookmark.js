const mysql = require("mysql2");
let pool, promisePool;

const init = () => {
  let { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;
  const poolOptions = {
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
  };

  pool = mysql.createPool(poolOptions);
  promisePool = pool.promise();
};

const end = () => {
  pool.end();
};

const get = async (user_id) => {
  const [rows, fields] = await promisePool.query(
    `SELECT 
        job.id,
        job.company_name,
        job.location,
        job.description,
        job.salary_range,
        job.skills,
        job.company_id 
    FROM bookmarks RIGHT JOIN jobs job on bookmarks.job_id = job.id
    WHERE bookmarks.user_id = ?`,
    [user_id]
  );

  return rows;
};

const getById = async (id) => {
  const [rows, fields] = await promisePool.query(
    `SELECT 
        job.id,
        job.company_name,
        job.location,
        job.description,
        job.salary_range,
        job.skills,
        job.company_id 
    FROM bookmarks RIGHT JOIN jobs job on bookmarks.job_id = job.id
    WHERE bookmarks.id = ? LIMIT 1`,
    [id]
  );

  return rows[0];
};

const store = async (item) => {
  const [rows, fields] = await promisePool.query(
    "INSERT INTO bookmarks (job_id,user_id) values(?,?)",
    [item.job_id, item.user_id]
  );

  return { ...item, id: rows.insertId };
};

const update = async (id, item) => {
  return ([rows, fields] = await promisePool.query(
    "UPDATE bookmarks SET job_id=?, user_id=? WHERE id=?",
    [item.job_id, item.user_id, id]
  ));
};

const remove = async (id) => {
  return ([rows, fields] = await promisePool.query(
    "DELETE FROM bookmarks WHERE id=?",
    [id]
  ));
};

module.exports = {
  init,
  end,
  get,
  getById,
  store,
  update,
  remove,
};
