const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dotenv = require("dotenv");
const { jobs, candidates, bookmarks, companies } = require("./routes");
const auth = require("./auth");

dotenv.config();

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.use("/api/auth", auth);
app.use("/api/jobs", jobs);
app.use("/api/candidates", candidates);
app.use("/api/bookmarks", bookmarks);
app.use("/api/companies", companies);

module.exports = app;
