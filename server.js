const app = require("./src/index");
const db = require("./src/database");

const PORT = 8000;

app.use("*", (req, res) => {
  res.status(404).send("<html><body>Resource not found</body></html>");
});

db.init()
  .then(() => {
    app.listen(PORT, () => console.log("Listening on port " + PORT));
    db.teardown()
      .then(() => {})
      .catch((err) => {
        console.log(err);
        process.exit(1);
      });
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
